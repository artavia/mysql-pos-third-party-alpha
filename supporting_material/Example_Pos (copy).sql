
-- -----------------
SET FOREIGN_KEY_CHECKS = 0;

-- -------------------
CREATE DATABASE IF NOT EXISTS `Example_Pos`
  CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `Example_Pos`;

-- -----------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (

  `id` BINARY(16) NOT NULL ,
  `name` VARCHAR(250) DEFAULT NULL ,
  `price` FLOAT(4) DEFAULT NULL ,
  `quantity` INTEGER NOT NULL ,

  CONSTRAINT pk_inventory PRIMARY KEY (id),
  UNIQUE KEY (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='' COLLATE=utf8_unicode_ci; -- 2023 NEW

-- -----------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  
  `id` BINARY(16) NOT NULL ,
  `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP ,
  `total` FLOAT(4) DEFAULT NULL , 
  `items` LONGTEXT DEFAULT NULL ,
  `tax` FLOAT(4) DEFAULT NULL ,

  CONSTRAINT pk_transactions PRIMARY KEY (id),
  UNIQUE KEY (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='' COLLATE=utf8_unicode_ci; -- 2023 NEW

-- -----------------
SET FOREIGN_KEY_CHECKS = 1;

-- -------------------
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -----------------

-- -----------------
-- insertion statements
-- -----------------

INSERT INTO Example_Pos.inventory (`id`,`name`,`price`,`quantity`) VALUES 
(UUID_TO_BIN(UUID()), 'Land Speeder (Electric)', 95.49, 10),
(0x14f722e0e4a911ed923677c7e1144f60, 'Busch Beer (6 pack)', 5.99, 8),
(UUID_TO_BIN(UUID()), 'Millenium Falcon (Diesel-operated, 1/8 scale)', 779.99, 10),
(UUID_TO_BIN(UUID()), 'Keg of Root Beer', 79.99, 10),
(UUID_TO_BIN(UUID()), 'Boxer shorts (3-pack Fuzzy Wuzzy brand)', 13.99, 10),
(0xd0c7b4cdded111edab34d9e5f0044345, 'Charanko Strap', 1.99, 5),
(UUID_TO_BIN(UUID()), 'Loguetown-brand Goggles', 299.99, 10),
(0xea9febf0e44c11edab4d734092348404, 'Rubber Chicken', 2.99, 6);

INSERT INTO Example_Pos.transactions (`id`, `total`, `items`, `tax`) VALUES 
(0xf03b88f0eae411ed9cfd27c5bdddfa94, 11.96, '[{\"ordinal\":0,\"id\":\"ea9febf0-e44c-11ed-ab4d-734092348404\",\"name\":\"Rubber Chicken\",\"price\":2.99,\"quantity\":4}]', 0.54),
(0xf77c3730e6b711ed91e437fd2c7781b9, 21.93, '[{\"ordinal\":0,\"id\":\"d0c7b4cd-ded1-11ed-ab34-d9e5f0044345\",\"name\":\"Charanko Strap\",\"price\":1.99,\"quantity\":5},{\"ordinal\":1,\"id\":\"14f722e0-e4a9-11ed-9236-77c7e1144f60\",\"name\":\"Busch Beer (6 pack)\",\"price\":5.99,\"quantity\":2}]', 0.99);

-- -----------------
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;