const express = require("express");
const { 
  Op, 
  fn, 
  col, 
  where,
} = require("sequelize");

const transactionsRouter = express.Router();

const Transaction = require("./../custom_models_sequelize/transaction.model");
const Inventory = require("./../custom_models_sequelize/inventory.model");

// =============================================
// ROUTING ASSIGNMENTS
// =============================================

// ## ##################
// ## GET all records - transactions
// http://127.0.0.1:5000/api/transactions/all
// ## ##################
transactionsRouter.get( "/all", async (req,res) => {
  
  const findAllOptions = { 
    attributes: {
      include: [ 
        [ fn( 'BIN_TO_UUID' , col('id'), ) , 'id' ] ,
        'date', 
        'total', 
        'items', 
        'tax', 
      ] ,
    } , 
    // order: [ [ 'id' , 'ASC' ] ] , 
    // order: [ [ 'id' , 'DESC' ] ] , 
  };

  await Transaction.findAll( findAllOptions )
  .then( transactions => {
    // console.log( '>>>>>> All records: ' , transactions );
    return res.status(200).json( transactions );
  } )
  .catch( ( err ) => {
    // console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } ); 
  } );

} );

// ## ##################
// FILTERED ARRAY and SORTED IN DESCENDING ORDER RETURNING TRANSACTIONS FOR THE PREVIOUS 24 HOURS
// ## GET transactions for the LAST 24 HOURS - transactions
// http://127.0.0.1:5000/api/transactions/todaysnumbers?date=MYFARRGINGDATE (?!?!?!)
// http://127.0.0.1:5000/api/transactions/todaysnumbers?date=2022-12-09
// ## ##################
transactionsRouter.get( "/todaysnumbers", async (req,res) => {

  // console.log("req.query.date" , req.query.date ); // typeof req.query.date === STRING  
  // console.log("req.query.date.length" , req.query.date.length ); 
  
  // beginning of query date day
  var startNum = new Date( await req.query.date).setHours(0,0,0,0); 
  var startDate = new Date( await startNum ); // await console.log("todaysnumbers startDate" , await startDate ); 
  
  // end of query date day
  var endNum = new Date( await req.query.date).setHours(23,59,59,999); 
  var endDate = new Date( await endNum ); // await console.log("todaysnumbers endDate" , await endDate ); 

  // return all items with date in question

  const findTodayOptions = await {
    attributes : {
      include: [ [ fn( 'BIN_TO_UUID' , col('id'), ) , 'id' ] , 'date' , 'total', 'items', 'tax' ] 
    } ,     
    raw: true , // NO MORE dataValues
    where: {
      date: { 
        [Op.and]: {
          [Op.gte]: startNum ,
          [Op.lte]: endNum , 
        }
      }
    } 
  };
  
  await Transaction.findAll( await findTodayOptions )
  .then( async docs => {
    if( await docs.length === 0 ){
      // await console.log( '\n\n', "docs.length === 0", await docs);
      return res.status(200).json( await docs );
    }
    else
    if( await docs.length > 0 ){
      // await console.log( '\n\n', "docs.length > 0", await docs);
      
      let shallowcopy = [ ...docs ]; // await console.log( "shallowcopy", await shallowcopy );

      const mappedTransactions = await shallowcopy.map( (el, idx, arr) => {
        return { ...el, date: new Date(el.date) }; 
      } ); // await console.log( "mappedTransactions", await mappedTransactions );

      const sortDesc = await mappedTransactions.sort( (a,b) => {
        return new Date( b.date ) - new Date( a.date );
      } ); // await console.log( "sortDesc", await sortDesc );

      const reducedSumOfAllSales = await sortDesc.reduce( ( accumulator, el, idx, arr ) => {
        return accumulator + el.total;
      } , 0 ); // await console.log( "reducedSumOfAllSales", await reducedSumOfAllSales ); // 662.05 

      const presentableTransactions = await sortDesc.map( (el, idx, arr) => { 
        return { ...el, date: `${el.date.toLocaleDateString()} ${el.date.toLocaleTimeString()}` }; 
      } ); // await console.log("presentableTransactions", await presentableTransactions );

      const responseObject = {
        docs: presentableTransactions ,
        numbers: reducedSumOfAllSales 
      };

      return res.status(200).json( await responseObject );
    }
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: \n\n` , JSON.stringify( err, null, 2 ) ); 
    return res.status(400).json( { errormessage: err } ); 
  } );

} );



// ## ##################
// FILTERED ARRAY and SORTED IN DESCENDING ORDER RETURNING TRANSACTIONS WITHIN A SPECIFIC RANGE OF DATES
// ## GET transactions for a particular date - transactions
// http://127.0.0.1:5000/api/transactions/by-date
// http://127.0.0.1:5000/api/transactions/customrange?startdate=1670112000000&enddate=1670371200000
// ## ##################
transactionsRouter.get( "/customrange", async (req,res) => {

  // console.log("req.query.startdate" , req.query.startdate ); // typeof req.query.startdate === STRING
  // console.log("req.query.enddate" , req.query.enddate ); // typeof req.query.enddate === STRING
  
  // if dates are provided
  if( !!( await req.query.startdate && await req.query.startdate >= 0) && !!( await req.query.enddate && await req.query.enddate >= 0) ){ 
    
    // beginning of query date day
    var startNum = new Date( parseInt( await req.query.startdate , 10 ) ).setHours(0,0,0,0); 
    var startDate = new Date( await startNum );
    
    // end of query date day
    var endNum = new Date( parseInt( await req.query.enddate , 10 ) ).setHours(23,59,59,999); 
    var endDate = new Date( await endNum );
  }
  
  // console.log("todaysnumbers startDate" , startDate ); 
  // console.log("todaysnumbers endDate" , endDate );   

  const customRangeOptions = await {
    attributes : {
      include: [ 
        [ fn( 'BIN_TO_UUID' , col('id'), ) , 'id' ] , 'date' , 'total', 'items', 'tax' 
      ] 
    } ,     
    raw: true , // NO MORE dataValues    
    where: {
      date: { 
        [Op.and]: {
          [Op.gte]: await startDate ,
          [Op.lte]: await endDate , 
        }
      }
    } 
  };

  await Transaction.findAll( await customRangeOptions )
  .then( async docs => {
    if( await docs.length === 0 ){
      // await console.log( '\n\n', "docs.length === 0", await docs);
      return res.status(200).json( await docs );
    }
    else
    if( await docs.length > 0 ){
      
      // await console.log( '\n\n', "docs.length > 0", await docs);
      
      let shallowcopy = [ ...docs ]; // await console.log( "shallowcopy", await shallowcopy );

      const mappedTransactions = await shallowcopy.map( (el, idx, arr) => {
        return { ...el, date: new Date(el.date) }; 

      } ); // await console.log( "mappedTransactions", await mappedTransactions );

      const sortDesc = await mappedTransactions.sort( (a,b) => {
        return new Date( b.date ) - new Date( a.date );
      } ); // await console.log( "sortDesc", await sortDesc );

      const reducedSumOfAllSales = await sortDesc.reduce( ( accumulator, el, idx, arr ) => {
        return accumulator + el.total;
      } , 0 ); // await console.log( "reducedSumOfAllSales", await reducedSumOfAllSales ); // 662.05 

      const presentableTransactions = await sortDesc.map( (el, idx, arr) => { 
        return { ...el, date: `${el.date.toLocaleDateString()} ${el.date.toLocaleTimeString()}` }; 
      } ); // await console.log("presentableTransactions", await presentableTransactions );

      const responseObject = {
        docs: presentableTransactions ,
        numbers: reducedSumOfAllSales 
      };

      return res.status(200).json( await responseObject );
    }
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: \n\n` , JSON.stringify( err, null, 2 ) ); 
    return res.status(400).json( { errormessage: err } ); 
  } );

} );

// ## ##################
// ## GET one record - transactions
// http://127.0.0.1:5000/api/transactions/:transactionId
// ## ##################
transactionsRouter.get( "/:transactionId", async (req,res) => {

  // await console.log( "req.params.transactionId" , await req.params.transactionId );

  const findParam = {
    attributes : {
      include: [ [ fn( 'BIN_TO_UUID' , col('id'), ) , 'id' ] , 'date' , 'total', 'items', 'tax' ] 
    } ,     
    raw: true , // NO MORE dataValues
    where: {
      id: fn( 'UUID_TO_BIN' , await req.params.transactionId )
    }
  };

  await Transaction.findOne( await findParam )
  .then( transaction => {
    if (transaction === null) {
      console.log('Not found!');
    } 
    else {
      // console.log( '>>>>>> transaction: ' , transaction );
      return transaction;
    }
  } )
  .then( rawTransaction => {
    // console.log( '>>>>>> One Record: ' , rawTransaction );
    var shallowcopy = Object.assign( {} , {...rawTransaction } , { items: JSON.parse( rawTransaction.items ) } ); 
    return res.status(200).json( shallowcopy );
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

} );

// ## ##################
// ## POST - create ONE record (transaction)
// TEST WITH...
// http://127.0.0.1:5000/api/transactions/new
// ## ##################

transactionsRouter.post( "/new" , async ( req, res ) => {

  const { id, date, total, items, tax } = await req.body;

  const newTransaction = { 
    id: fn( 'UUID_TO_BIN' , await id ) , 
    date: await date , 
    total: await total , 
    items: await items , 
    tax: await tax , 
  };
  
  // console.log("\n newTransaction", newTransaction );

  const transaction = await Transaction.create( newTransaction ); 

  try{
    if( await transaction) {
      // await console.log( '\n\n' , "New transaction successfully completed!" );
      // await console.log( "transaction", await transaction );
      res.status(200).json( { message : "New transaction has been added" } );
    }
  }
  catch(err){
    // console.log( '\n\n' , "Transaction not completed!" );
    // console.log( "err", err);
    // console.log( `There was derrpage with NEW recipes: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  }
  // finally{
  if( await transaction.items !== undefined ){
    // await console.log( "transaction.items", await transaction.items );
    Inventory.decrementInventory( JSON.parse( await transaction.items ) ); 
  }
  // }

} );

module.exports = transactionsRouter;