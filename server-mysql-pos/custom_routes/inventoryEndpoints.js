const express = require("express");
const { 
  Op, 
  fn, 
  col, 
  where,
} = require("sequelize");

const inventoryRouter = express.Router();
const Inventory = require("./../custom_models_sequelize/inventory.model");

// =============================================
// ROUTING ASSIGNMENTS
// =============================================

// ## ##################
// ## GET a product from inventory by productId - product
// http://127.0.0.1:5000/api/inventory/product/:productId
// ## ##################
inventoryRouter.get( "/product/:productId", async (req,res) => {
  
  // await console.log( "req.params.productId" , await req.params.productId );
  
  if( await !req.params.productId ){
    return res.status(500).json( { errormessage: "ID field is required" } );
  }
  else {
    
    const findOneCurrentParam = {
      attributes : {
        include: [ [ fn( 'BIN_TO_UUID' , col('id'), ) , 'id' ] , 'name', 'price', 'quantity' ] 
      } ,     
      raw: true , // NO MORE dataValues
      where: {
        id: fn( 'UUID_TO_BIN' , await req.params.productId )
      }
    };
    
    await Inventory.findOne( await findOneCurrentParam )
    .then( product => {
      if (product === null) {
        console.log('Not found!');
      } 
      else {
        return product;
      }
    } )
    .then( currentproduct => {
      // console.log( '>>>>>> One Record: ' , currentproduct );
      return res.status(200).json( currentproduct );
    } )
    .catch( ( err ) => {
      // console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
      return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
    } );
    
  }  
} );

// ## ##################
// ## GET all records - product
// http://127.0.0.1:5000/api/inventory/products
// ## ##################

inventoryRouter.get( "/products" , async ( req, res ) => { 

  const findAllOptions = { 
    attributes: {
      include: [ 
        [ fn( 'BIN_TO_UUID' , col('id'), ) , 'id' ] ,
        'name', 
        'price', 
        'quantity', 
      ] ,
    } , 
    // order: [ [ 'id' , 'ASC' ] ] , 
    // order: [ [ 'id' , 'DESC' ] ] , 
  };

  await Inventory.findAll( findAllOptions )
  .then( products => {
    // console.log( '>>>>>> All record: ' , products );
    return res.status(200).json( products );
  } )
  .catch( ( err ) => {
    // console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } ); 
  } );
  
} );

// ## ##################
// ## POST - create ONE record (product) - 
// http://127.0.0.1:5000/api/inventory/product
// ## ##################

inventoryRouter.post( "/product" , async ( req, res ) => {

  const { id, name, quantity, price } = await req.body;
  
  var newproduct = { 
    id: fn( 'UUID_TO_BIN' , await id ) , 
    name: await name , 
    quantity: await quantity ,
    price: await price ,
  };

  // console.log("\n newproduct", newproduct );

  await Inventory.create( newproduct )
  .then( new_product => {
    // console.log( '>>>>>> New record added: ' , new_product );
    return res.status(200).json( { message : "New record has been added" } );
  } )
  .catch( ( err ) => { 
    // console.log( `There was derrpage with NEW products: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

} );

// ## ##################
// ## UPDATE - update ONE record (product)
// http://127.0.0.1:5000/api/inventory/product
// ## ##################

inventoryRouter.put( "/product" , async ( req, res ) => {
  
  const { id, name, quantity, price } = await req.body;
  
  const existingproduct = { 
    name: await name , 
    quantity: await quantity ,
    price: await price ,
  };

  const findParam = {
    where: {
      id: fn( 'UUID_TO_BIN' , await id )
    }
  };
  
  Inventory.findOne( findParam )
  .then( product => {
    if (product === null) {
      console.log('Not found!');
    } 
    else {
      // console.log( "product instanceof Inventory", product instanceof Inventory ); // true
      // console.log( '>>>>>> product: ' , product );
      return product;
    }
  } )
  .then( product => {

    var optionsPm = {
      where: {
        id: { 
          [Op.eq]: fn( 'UUID_TO_BIN' , product.id ) 
        }
      } 
    };
    
    return product.update( existingproduct , optionsPm );
  } )
  .then( product => {
    // console.log( '>>>>>> Record updated: ' , product );
    return res.status(200).json( product );
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );  

} );

// ## ##################
// ## custom method
// ## ##################


module.exports = inventoryRouter;