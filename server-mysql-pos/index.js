// =============================================
// BASE SETUP
const express = require( 'express' );
const app = express();
const http = require("node:http"); 

var liveCart = [];

const { Server : SocketIOServer } = require("socket.io"); 
const cors = require("cors");
const helmet = require("helmet");
const path = require("node:path");

// =============================================
// process.env SETUP

if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { 
  NODE_ENV , 
  PORT , 
  SERVERHOSTNAME ,
} = process.env;

// =============================================
// Establish the NODE_ENV toggle settings
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production'; // true
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development'; // true

// =============================================
// IMPORT DB MIDDLEWARE AUTHENTICATION PROMISE
const connectDB = require("./custom_auth_promise/authentication");

// =============================================
// ROUTER SETUP
const transactionsRouter = require("./custom_routes/transactionsEndpoints");
const inventoryRouter = require("./custom_routes/inventoryEndpoints");

// =============================================
// APP SETUP - USE PARAMS - FINER DETAILS

var corsOptions = {  
  // "origin": "*" , // The default configuration is the equivalent of 1/4
  // "origin": 'http://127.0.0.1:3000/' , // TRUTHY!
  "origin": [`http://localhost:3000`, `http://127.0.0.1:3000`, `http://localhost:5000`, `http://127.0.0.1:5000`] , // YEPPERS!
  "optionsSuccessStatus": 204 ,  // The default configuration is the equivalent of 2/4
  // "optionsSuccessStatus": 200 , // some legacy browsers (IE11, various SmartTVs) choke on 204
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE" , // The default configuration is the equivalent of 3/4
  "preflightContinue": false , // The default configuration is the equivalent of 4/4
}; 

app.use( cors( corsOptions ) ); 
app.use( express.json() ); 
app.use( express.urlencoded( { extended: true } ) ); 

// ----------------------------------------------------------------------
// | helmet                                                              |
// ----------------------------------------------------------------------
const initHelmetConfigObj = {
  expectCt : false , // DEPRECATED
  hsts : false , // disables the middleware but keep the rest
};

const helmetCspConfig = {  
  // useDefaults: false , // Sets all of the defaults, but makes exceptions with overrides
  directives: {
    "default-src": ["'self'"] , 
    "base-uri": ["'self'"] , 
    "font-src": ["'self'", "https:", "data:"] ,
    "form-action": ["'self'"] , 
    "frame-ancestors": ["'self'"] , 
    "img-src": ["'self'", "data:"] ,
    "media-src": ["'self'"] , 
    "object-src": ["'none'"] ,
    "script-src": ["'self'"] , 
    "script-src-attr": ["'none'"] , 
    "style-src": ["'self'", "https:", "'unsafe-inline'"] ,
    "connect-src": ["*", "data:", "blob:", "filesystem:"] ,
    upgradeInsecureRequests: [] ,
  } ,
};

app.use(
  helmet( initHelmetConfigObj ) ,
  helmet.contentSecurityPolicy( helmetCspConfig ) ,
  helmet.crossOriginEmbedderPolicy( { policy: "require-corp" , } ) ,
  helmet.crossOriginOpenerPolicy() ,
  helmet.crossOriginResourcePolicy() ,
  helmet.referrerPolicy( { policy: "no-referrer", } ) , 
  helmet.noSniff() , 
  helmet.originAgentCluster() , 
  helmet.dnsPrefetchControl({ allow: false , }) , 
  helmet.ieNoOpen() , 
  helmet.frameguard( { action: "deny" , } ) , 
  helmet.permittedCrossDomainPolicies( { permittedPolicies: "none", } ) ,
  helmet.hidePoweredBy() , 
  helmet.xssFilter() 
);

// ----------------------------------------------------------------------
// | trust proxy setting                                                |
// | this permits the assignment of secure cookies which CAN choke
// | due to, for example, express-session which is NOT APPLICABLE today
// ----------------------------------------------------------------------
app.enable( 'trust proxy' , 1 ); 

// ----------------------------------------------------------------------
// | Server-side technology information                                 |
// | Remove the `X-Powered-By` response header that
// | contributes to header bloat and can unnecessarily expose vulnerabilities
// | This was causing a glitch b/t client and server sockets
// | helmet, apparently, is the go-to middleware to address this issue!
// ----------------------------------------------------------------------
// app.disable( 'x-powered-by' ); // old hat

// ----------------------------------------------------------------------
// | ETags                                                              |
// | PERFORMANCE helper function
// | Remove `ETags` as resources are sent with far-future expires headers
// | https://developer.yahoo.com/performance/rules.html#etags
// ----------------------------------------------------------------------
app.set( 'etag' , false );

// =============================================
// Log the request
app.use( ( req, res, next) => {
  console.info( '\n', `METHOD: [${req.method}] - URL: [ ${req.url} ] - IP: [ ${req.socket.remoteAddress } ]`);
  res.on( 'finish', () => {
    console.info( '\n', `METHOD: [${req.method}] - URL: [ ${req.url} ] - STATUS: [ ${ res.statusCode } ] - IP: [ ${req.socket.remoteAddress } ]`);
  } );
  next();
} );


// =============================================
// Server variables setup
const httpServer = http.createServer( app );

// =============================================
// websocket variables setup 

const serversocketconfig = {  
  cookie: false , 
  cors: {     
    allowedHeaders: [
      "my-custom-header", 
      "Origin", "Content-type", "Accept", 
      "X-Requested-With" , "Authorization" , 
      "Access-Control-Allow-Origin" , 
    ] , 
  } ,
  pingInterval: 10000 ,
  pingTimeout: 5000 ,
  serveClient: false ,
};

const io = new SocketIOServer( httpServer, serversocketconfig );

// =============================================
// APPLY THE ROUTES TO THE APP
// =============================================

// ## ##################
// ## GET Healthcheck
// http://127.0.0.1:5000/ping
// ## ##################
app.get('/ping', ( req, res, next ) => {
  return res.json( { konichiwa: "WHIRLLED WORLD!!!" } );
} );

// ## ##################
// ## GET inventory
// http://127.0.0.1:5000/
// PRODUCTION purposes ONLY
// ## ##################
app.get('/', ( req, res, next ) => {  
  if( (IS_PRODUCTION_NODE_ENV === true) ){ 
    
    var options = {
      root: path.join( __dirname, 'public') ,
      dotfiles: 'deny' ,
      headers: { 'x-timestamp': Date.now(), 'x-sent': true } ,
    };    
    var fileName = 'index.html'; 
    res.sendFile( fileName, options, function (err) {
      if (err) {
        next(err);
      }
    });
    next();

  }
} );

app.use( "/api/transactions", transactionsRouter );
app.use( "/api/inventory", inventoryRouter); 

// =============================================
// Serve static files from the React app
// Be sure that NODE_ENV is configured to "production" in the server dot env file
// or else "Error: ENOENT:" will show up in the server-side terminal
// =============================================
if( (IS_PRODUCTION_NODE_ENV === true) ){
  app.use( express.static( path.join( __dirname, 'public' ) ) ); 
}

// =============================================
// The "catchall" handler that will handle all requests in order to return React's index.html file.
// Be sure that NODE_ENV is configured to "production" in the server dot env file
// or else "Error: ENOENT:" will show up in the server-side terminal
// =============================================
const catchAllGET = ( req, res, next ) => {
  res.sendFile( path.join( __dirname , 'public', 'index.html' ) );
};
if( (IS_PRODUCTION_NODE_ENV === true) ){
  app.get( '*' , catchAllGET ); 
}

// =============================================
// Websocket logic 

io.on( "connection" , (ws) => {

  console.log(`SocketIOServer instance - User Connected...: ${ws.id}`);
  
  
  ws.on( "live-cart-page-loaded", () => { // upon page load, give user current cart... On page load, make client update live cart 
    // console.log( "live-cart-page-loaded - liveCart", liveCart ); 
    ws.emit("update-live-cart-display", liveCart );
  } );  
  
  ws.on( "update-live-cart", (cartData) => { // fires when the cart data is updated by POS

    // console.log( "update-live-cart - cartData", cartData );
    
    liveCart = cartData; // keep track of live cart
    console.log( '\n', "update-live-cart - liveCart", liveCart ); 

    // broadcast updated live cart to all websocket clients
    ws.broadcast.emit("update-live-cart-display", liveCart ); // USE FOR NORMAL LIVE PURPOSES

  } );

  ws.on("disconnect", (reason) => {
    console.log( '\n', "disconnect reason: " , reason );
    console.log( `SocketIOServer instance - User disconnected...: ${ws.id}`); 
  } );

} );

// =============================================
// DB and BACKEND SERVERS INSTANTIATION
connectDB().then( async () => { 
  await httpServer.listen( PORT, SERVERHOSTNAME, null, () => {
    if( IS_PRODUCTION_NODE_ENV === true ){
      console.log( `Backend is running at http://${SERVERHOSTNAME}:${PORT}/ baby.`);
    }
    if( IS_DEVELOPMENT_NODE_ENV === true ){      
      console.log( `Development backend has started at http://${SERVERHOSTNAME}:${PORT}/api/ .`);
    }
    if( IS_PRODUCTION_NODE_ENV === false && IS_DEVELOPMENT_NODE_ENV === false ){ 
      console.log(`Development backend is now testing at port ${PORT} . Run and visit your frontend.`);
    }    
  } ); // PORT, hostname, backlog (null), cb 
} );