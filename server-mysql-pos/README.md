# MySQL Point of Sale (&quot;Alpha&quot;) - server

## Description
A simulated point of sale application that uses ExpressJS, MySQL, Sequelize and ReactJS.

## Visit the accompanying project
This project is the subsequent iteration of the accompanying work I have published at [gitlab](https://gitlab.com/artavia/pos-third-party-alpha/). You can read more about it there if you like.

## Processing and/or completion date(s)
The [journal](https://gitlab.com/artavia/mysql-pos-third-party-alpha/-/blob/main/supporting_material/server-mysql-pos-JOURNAL.md) detailing processing dates is set apart due to it's monotonous nature.

## Purpose behind this project
The [background and justification](https://gitlab.com/artavia/mysql-pos-third-party-alpha/-/blob/main/supporting_material/BACKGROUND.md) details the rationale behind this project.

## Instructions

I have included a sample database to be found in the **supporting_material** folder. Evidently, you need to have MySQL version 8.0 installed and running for best results&hellip; in addition to the other requisite programs such as NodeJS, too.

In order to operate the development environment, you will have **two terminals** open (front and back respectively). 

On the server side, adjust the NODE_ENV in the .env file and use the appropriate script preset. I prefer **yarn** but **I used npm**. Run **npm install** in the server folder, then, you're off!

On the client side, just run &quot;**npm run start**&quot; as you would normally do. Make sure to first run **npm install** in the client folder, then, you can launch the front-end, too!

It all works as expected. You can even use &quot;**npm run build**&quot; in the front-end and the output will be tidily placed in the back-end.

### May Jesus bless you
That is all I have to say for now. I hope to have influenced you in some positive manner today. God bless.