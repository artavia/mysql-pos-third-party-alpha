// =============================================
// DATABASE SETUP 
// QUERY MODIFIERS
// https://sequelize.org/master/manual/getting-started.html#tip-for-reading-the-docs
// =============================================

const { Sequelize } = require("sequelize");

const dbconstant = "Example_Pos"; 
const usernameconstant = "root"; 
const userpasswordconstant = "password"; 
const configurationObj = { 
  "dialect": "mysql", 
  "host": "localhost" ,
  "define": {
    "timestamps": false
  },
};

const sequelize = new Sequelize( dbconstant, usernameconstant, userpasswordconstant, configurationObj );

module.exports = sequelize;