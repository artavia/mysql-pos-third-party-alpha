import React, { useRef, useCallback, useEffect } from 'react';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import { v1 as uuidv1 } from 'uuid';

import { Product } from '../pages/Product';
import { NoProductDisclosure } from '../elements/tablerow_static/noproductsdisclosure';
import { ProductsTable } from '../elements/table/products-table';
import { AddProductForm } from '../elements/form/add-product-form';
import SharedModal from '../shared/shared-modal';

const ProductViewer = ( props ) => {

  // console.log( "props", props );
  // const HOST = `http://127.0.0.1:5000/`; 

  const addModalRef = useRef(null);

  const addFormNameInput = useRef(null);
  const addFormPriceInput = useRef(null);
  const addFormQuantityInput = useRef(null);

  const getProducts = async () => { 
    
    // http://127.0.0.1:5000/api/inventory/products

    const baseUrl = `/api/inventory/products`; 
    const result = await axios.get( baseUrl );
    
    // await console.log( "result.data", await result.data );

    if( props.productsProperties.products !== await result.data ){
      props.productsProperties.setProducts( await result.data );
    }
  };

  const handleSnackbar = ()=>{
    var $snackbar = props.snackbarProperties.snackbar.current;
    $snackbar.className = "show";
    setTimeout( function(){
      $snackbar.className = $snackbar.className.replace("show", "");
    }, 2812.5);
  };

  const fetchData = useCallback( async () => {
    await getProducts();
    // eslint-disable-next-line
  } , [ ] );

  useEffect( () => {
    fetchData();
    // eslint-disable-next-line
  } , [ ] );

  const handleNewProduct = (event) => {

    event.preventDefault(); 
    
    // console.log("KONICHIWA!!!");

    const addFormData = new FormData();
    addFormData.append("name", addFormNameInput.current.value );
    addFormData.append("quantity", addFormQuantityInput.current.value );
    addFormData.append("price", addFormPriceInput.current.value );

    var newProduct = {
      id: uuidv1() ,
      name: addFormData.get("name") ,
      quantity: parseInt( addFormData.get("quantity") , 10 ) ,
      price: Number( parseFloat( addFormData.get("price") ).toFixed(2) )
    }; 

    // console.log( "newProduct", newProduct );

    var createOptions = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8' ,
        "Accept": 'application/json' ,
      },
    }; 

    // http://127.0.0.1:5000/api/inventory/product
    
    axios.post( `/api/inventory/product` , JSON.stringify( newProduct ), createOptions )
    .then( (response) => { 
      // console.log( "response", response );
      props.snackbarProperties.setSnackMessage("PRODUCT ADDED SUCCESSFULLY.");
      handleSnackbar();
      getProducts(); // new
      addModalRef.current.exportedCloseAddModal();
    } )
    .catch( (error) => { 
      // console.log( "error", error );
      props.snackbarProperties.setSnackMessage("PRODUCT COULD NOT BE SAVED.");
      handleSnackbar();
      addModalRef.current.exportedCloseAddModal();
    } );
    /**/

  }; 

  // #################
  const mapProducts = (product, idx, arr) => {
    
    const productProperties = {
      key: idx , 
      product: product , 
    };

    const additionalProductProperties = {
      supplementalProperties: {
        getProducts: getProducts ,
        handleSnackbar: handleSnackbar ,
        setSnackMessage: props.snackbarProperties.setSnackMessage ,
      }
    };
    
    return <Product { ...productProperties } { ...additionalProductProperties } />
  };

  const productsList = () => {
    return props.productsProperties.products.map( mapProducts );
  };

  // #################
  const openAddModalHandler = () => addModalRef.current.exportedShowAddModal();

  // #################  
  const addFormProperties = {
    addFormNameInput: addFormNameInput ,
    addFormQuantityInput: addFormQuantityInput , 
    addFormPriceInput: addFormPriceInput , 
    handleNewProduct: handleNewProduct , 
  };

  // #################
  const addModalProperties = {
    ref: addModalRef ,
    custommodalscope: "addproduct",
  };

  // #################  

  let element = (
    <>
      <div className="container">
        <div>
          <Button variant="info" className={'btn btn-success float-end'} onClick={ () => openAddModalHandler() }>
            <i className="fas fa-plus"></i>&nbsp; Add New Item
          </Button>
          <br />
          <br />
          <ProductsTable>
            { props.productsProperties.products.length > 0 ? productsList() : (<NoProductDisclosure />) }
          </ProductsTable>
          <SharedModal { ...addModalProperties }>
            <AddProductForm { ...addFormProperties } />
          </SharedModal>
          <div id='snackbar' ref={ props.snackbarProperties.snackbar }>{ props.snackbarProperties.snackMessage }</div>
        </div>
      </div>
    </>
  );
  return element;
};

export {ProductViewer}; 