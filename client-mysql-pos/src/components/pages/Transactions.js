import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';

import { NoTransactionsDisclosure } from '../elements/tablerow_static/notransactionsdisclosure';
import { CompleteTransactions } from '../elements/tablerow_repeating/CompleteTransactions';

const Transactions = (props) => {

  // console.log( "props", props );

  // const HOST = `http://127.0.0.1:5000/`;
  
  let [ transactions, setTransactions ] = useState([]);

  const getTransactions = async () => {
    const baseUrl = `/api/transactions/all`; // http://127.0.0.1:5000/api/transactions/all 
    const result = await axios.get(baseUrl);

    try{
      if( transactions !== await result.data ){
        
        // console.log( "result.data", result.data );

        const sortDesc = await result.data.sort( (a,b) => {
          return new Date( b.date ) - new Date( a.date );
        } );
        setTransactions( await sortDesc );
      }
    }
    catch(err){
      console.log( "err", err );
    }
  }

  const fetchData = useCallback( async () => {
    await getTransactions();
    // eslint-disable-next-line
  } , [] );

  useEffect( () => {
    fetchData();
    // eslint-disable-next-line
  } , [] );

  const mapTransactions = (transaction, idx, arr) => {
    
    var newProps = {
      key: idx , 
      transaction: transaction , 
    };
    
    return <CompleteTransactions {...newProps } />;
  };

  const renderCompleteTransactions = () => {
    return transactions.map( mapTransactions );
  };
  
  let element = (
    <>
      <div className='container'>
        <table className='transactions table table-responsive table-striped table-hover'>
          <thead>
            <tr className='titles'>
              <th>Transaction ID</th>
              <th>Time</th>
              <th>Sub&#45;Total</th>
              <th>Sales Tax</th>
              <th>Items</th>
              <th>Show Items</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            { transactions.length > 0 ? renderCompleteTransactions() : (<NoTransactionsDisclosure />) } 
          </tbody>
        </table>
      </div>
    </>
  );

  return element;
};

export { Transactions };
